#include <chrono>
#include <functional>
#include <iostream>

// #define USE_BOOST_IN_TESTS

#ifdef USE_BOOST_IN_TESTS
#include <boost/function.hpp>
#endif

#include "Event.h"
#include "Lambda.h"

void TestFunc(std::string text, std::string text2)
{
	std::cout << text << ", " << text2 << '\n';
}

int TestFuncInt(std::string text, std::string text2)
{
	std::cout << text << ", " << text2 << '\n';
	return 0;
}

void TestFuncConstRef(const std::string& text, const std::string& text2)
{
	std::cout << text << ", " << text2 << '\n';
}

class TestClass
{
public:
	void Print()
	{
		TestFunc("I am a method and I print something", "here");
	}

	void TestMeth(std::string text, std::string text2)
	{
		TestFunc(text, text2);
	}

	void TestMethConstRef(const std::string& text, const std::string& text2)
	{
		TestFunc(text, text2);
	}

	void TestMethConst(std::string text, std::string text2) const
	{
		TestFunc(text, text2);
	}

	void TestMethConstRefConst(const std::string& text, const std::string& text2) const
	{
		TestFunc(text, text2);
	}

	void SpeedTestMethod(const std::string& text)
	{
		auto newText = text + " yeah";
	}

	void SpeedTestMethodRefless(std::string text)
	{
		auto newText = text + " yeah";
	}

	static void TestStaticMeth(std::string text, std::string text2)
	{
		TestFunc(text, text2);
	}

	static void TestStaticMethConstRef(const std::string& text, const std::string& text2)
	{
		TestFunc(text, text2);
	}
};

void SpeedTestFunc(const std::string& text)
{
	auto newText = text + " yeah";
}

void SpeedTestWithFunc(long long loopCount)
{
	using namespace std::chrono;
	const std::string testText = "testText";

	std::cout << "speed test on functions with " << loopCount << " loops:" << '\n';
	std::cout << "using calls without explicit refs:" << '\n';
	{
		std::cout << "\tnew delegate call duration: ";
		const Delegate::Delegate<void(std::string)> speedTestDelegate = &SpeedTestFunc;
		auto beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestDelegate(testText);
		}
		auto afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';

		std::cout << "\tstd::function call duration: ";
		const std::function<void(std::string)> speedTestStdFunc = &SpeedTestFunc;
		beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestStdFunc(testText);
		}
		afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';

#ifdef USE_BOOST_IN_TESTS
		std::cout << "\tboost::function call duration: ";
		const boost::function<void(std::string)> speedTestBoostFunction = &SpeedTestFunc;
		beforeCallTest = system_clock::now();
		for (int i = 0; i < loopCount; ++i)
		{
			speedTestBoostFunction(testText);
		}
		afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';
#endif
	}
	std::cout << "using calls with explicit refs:" << '\n';
	{
		std::cout << "\tnew delegate call duration: ";
		const Delegate::Delegate<void (const std::string&)> speedTestDelegate(&SpeedTestFunc);
		auto beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestDelegate(testText);
		}
		auto afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';

		std::cout << "\tstd::function call duration: ";
		const std::function<void(const std::string&)> speedTestStdFunc = &SpeedTestFunc;
		beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestStdFunc(testText);
		}
		afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';

#ifdef USE_BOOST_IN_TESTS
		std::cout << "\tboost::function call duration: ";
		const boost::function<void(const std::string&)> speedTestBoostFunction = &SpeedTestFunc;
		beforeCallTest = system_clock::now();
		for (int i = 0; i < loopCount; ++i)
		{
			speedTestBoostFunction(testText);
		}
		afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';
#endif
	}
}

void SpeedTestWithMeth(long long loopCount)
{
	using namespace std::chrono;
	const std::string testText = "testText";
	TestClass testClass;

	std::cout << "speed test on methods with " << loopCount << " loops:" << '\n';
	std::cout << "using calls without explicit refs:" << '\n';
	{
		std::cout << "\tnew delegate call duration: ";
		const Delegate::Delegate<void(std::string)> speedTestDelegate(&testClass, &TestClass::SpeedTestMethod);
		auto beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestDelegate(testText);
		}
		auto afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';

		std::cout << "\tstd::function call duration: ";
		const std::function<void (TestClass*, std::string)> speedTestStdFunc = &TestClass::SpeedTestMethod;
		beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestStdFunc(&testClass, testText);
		}
		afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';

#ifdef USE_BOOST_IN_TESTS
		std::cout << "\tboost::function call duration: ";
		const boost::function<void(std::string)> speedTestBoostFunction = [&testClass](auto&& text)
		{
			testClass.SpeedTestMethod(std::forward<decltype(text)>(text));
		};
		beforeCallTest = system_clock::now();
		for (int i = 0; i < loopCount; ++i)
		{
			speedTestBoostFunction(testText);
		}
		afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';
#endif
	}
	std::cout << "using calls with explicit refs:" << '\n';
	{
		std::cout << "\tnew delegate call duration: ";
		const Delegate::Delegate<void (const std::string&)> speedTestDelegate(
			&testClass, &TestClass::SpeedTestMethod);
		auto beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestDelegate(testText);
		}
		auto afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';

		std::cout << "\tstd::function call duration: ";
		const std::function<void(const std::string&)> speedTestStdFunc = [&testClass](auto&& text)
		{
			testClass.SpeedTestMethod(std::forward<decltype(text)>(text));
		};
		beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestStdFunc(testText);
		}
		afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';

#ifdef USE_BOOST_IN_TESTS
		std::cout << "\tboost::function call duration: ";
		const boost::function<void(const std::string&)> speedTestBoostFunction = [&testClass](auto&& text)
		{
			testClass.SpeedTestMethod(std::forward<decltype(text)>(text));
		};
		beforeCallTest = system_clock::now();
		for (int i = 0; i < loopCount; ++i)
		{
			speedTestBoostFunction(testText);
		}
		afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';
#endif
	}
}

void MulDelSpeedTestWithFunc(long long loopCount)
{
	using namespace std::chrono;
	const std::string testText = "testText";

	std::cout << "speed test of multicast holding 10 functions with " << loopCount << " loops:" << '\n';
	std::cout << "using calls without explicit refs:" << '\n';
	{
		std::cout << "\tnew multicast call duration: ";
		const Delegate::MulticastDelegate<void(std::string)> speedTestDelegate =
		{
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
		};
		auto beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestDelegate(testText);
		}
		auto afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';
	}

	std::cout << "using calls with explicit refs:" << '\n';
	{
		std::cout << "\tnew multicast call duration: ";
		const Delegate::MulticastDelegate<void (const std::string&)> speedTestDelegate =
		{
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
			&SpeedTestFunc,
		};
		auto beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestDelegate(testText);
		}
		auto afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';
	}
}

void MulDelSpeedTestWithMeth(long long loopCount)
{
	using namespace std::chrono;
	TestClass testClass;
	const std::string testText = "testText";

	std::cout << "speed test of multicast holding 10 methods with " << loopCount << " loops:" << '\n';
	std::cout << "using calls without explicit refs:" << '\n';
	{
		std::cout << "\tnew multicast call duration: ";
		const Delegate::MulticastDelegate<void(std::string)> speedTestDelegate =
		{
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
		};
		auto beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestDelegate(testText);
		}
		auto afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';
	}

	std::cout << "using calls with explicit refs:" << '\n';
	{
		std::cout << "\tnew multicast call duration: ";
		const Delegate::MulticastDelegate<void (const std::string&)> speedTestDelegate =
		{
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
			{&testClass, &TestClass::SpeedTestMethod},
		};
		auto beforeCallTest = system_clock::now();
		for(int i = 0; i < loopCount; ++i)
		{
			speedTestDelegate(testText);
		}
		auto afterCallTest = system_clock::now();
		std::cout << duration_cast<milliseconds>(afterCallTest - beforeCallTest) << '\n';
	}
}

void CallTestsOnParams()
{
	TestClass testClass;

	std::cout << "testing calls with different parameters type :" << '\n';
	Delegate::Delegate<void (std::string, std::string)> delegate = &TestFunc;
	delegate("Func", "ok");

	delegate = &TestFuncConstRef;
	delegate("Func const ref", "ok");

	delegate = Delegate::Delegate<void(std::string, std::string)>(&testClass, &TestClass::TestMeth);
	delegate("Method", "ok");

	delegate = Delegate::Delegate<void(std::string, std::string)>(&testClass, &TestClass::TestMethConstRef);
	delegate("Method const ref", "ok");

	delegate = &TestClass::TestStaticMeth;
	delegate("Static Method", "ok");

	delegate = &TestClass::TestStaticMethConstRef;
	delegate("Static Method const ref", "ok");

	delegate = Delegate::Delegate<void(std::string, std::string)>(&testClass, &TestClass::TestMethConst);
	delegate("Method const", "ok");

	delegate = Delegate::Delegate<void(std::string, std::string)>(&testClass, &TestClass::TestMethConstRefConst);
	delegate("Method const ref const", "ok");

	delegate = LAMBDA([] (std::string text, std::string text2) { TestFunc(text, text2); });
	delegate("Lambda w/out capture", "ok");

	delegate = LAMBDA([] (const std::string& text, const std::string& text2) { TestFunc(text, text2); });
	delegate("Lambda w/out capture const ref", "ok");

	delegate = LAMBDA([&] (std::string text, std::string text2) { TestFunc(text, text2); });
	delegate("Lambda w/ capture", "ok");

	delegate = LAMBDA([&] (const std::string& text, const std::string& text2) { TestFunc(text, text2); });
	delegate("Lambda w/ capture const ref", "ok");

	Delegate::Delegate<void(std::string&)> delWithRef = LAMBDA(
		[] (std::string& text)
		{
		TestFunc(text, "");text = "and has been modified";
		});

	std::string text = "this text was pass as non const reference";
	delWithRef.Invoke(text);
	delWithRef.Invoke(text);
}

void ComparisonsTests()
{
	TestClass testClass;
	Delegate::Delegate<void(std::string, std::string)> del1(&testClass, &TestClass::TestMethConst);
	Delegate::Delegate<void(std::string, std::string)> del2(&testClass, &TestClass::TestMethConst);
	Delegate::Delegate<void(std::string, std::string)> del3(&testClass, &TestClass::TestMethConstRef);

	std::cout << "method binded delegate comparison" << '\n';
	std::cout << "del1 == del2 (should be 1) => " << (del1 == del2) << '\n';
	std::cout << "del1 == del3 (should be 0) => " << (del1 == del3) << '\n';

	del1 = &TestFunc;
	del2 = &TestFunc;
	del3 = &TestFuncConstRef;

	std::cout << "function binded delegate comparison" << '\n';
	std::cout << "del1 == del2 (should be 1) => " << (del1 == del2) << '\n';
	std::cout << "del1 == del3 (should be 0) => " << (del1 == del3) << '\n';

	del1 = LAMBDA([] (std::string text, std::string text2) { TestFunc(text, text2); });
	del2 = LAMBDA([] (std::string text, std::string text2) { TestFunc(text, text2); });
	del3 = LAMBDA([] (std::string text, std::string text2) { TestFuncConstRef(text, text2); });

	//TODO: fix this problem => the Function object never reaches to the Lambda comparison as it couldn't cast the other to the correct type
	std::cout << "lambda binded delegate comparison" << '\n';
	std::cout << "del1 == del2 (should be 1) => " << (del1 == del2) << '\n';
	std::cout << "del1 == del3 (should be 0) => " << (del1 == del3) << '\n';

	del1 = Delegate::Delegate<void(std::string, std::string)>(&testClass, &TestClass::TestMethConst);
	del2 = &TestFunc;
	del3 = LAMBDA([] (std::string text, std::string text2) { TestFuncConstRef(text, text2); });

	std::cout << "any binding delegate comparison" << '\n';
	std::cout << "del1 == del2 (should be 0) => " << (del1 == del2) << '\n';
	std::cout << "del1 == del3 (should be 0) => " << (del1 == del3) << '\n';
	std::cout << "del2 == del3 (should be 0) => " << (del2 == del3) << '\n';
}

void Title(const std::string& text)
{
	std::cout << '\n' << text;
}

void a(const std::string&)
{
	std::cout << 'a';
}

void b(const std::string&)
{
	std::cout << 'b';
}

void MulDelCallTestsOnParams()
{
	using namespace Delegate;
	TestClass testClass;
	TestClass testClass2;
	MulticastDelegate<void(std::string, std::string)> mulDel;

	mulDel.AddListener(&TestFunc);
	mulDel.AddListener({&testClass, &TestClass::TestMethConst});
	mulDel.AddListener(LAMBDA([] (std::string a, std::string b) { TestFunc(a, b); }));
	mulDel.AddListener(LAMBDA([&] (const std::string& a, const std::string& b) { TestFunc(a, b); }));
	mulDel.Invoke("This message should appear ", "4 times");

	mulDel = &TestFuncInt;
	mulDel("And now this message should appear ", "1 time this time");

	mulDel += {&testClass, &TestClass::TestMethConst};
	mulDel += {&testClass2, &TestClass::TestMethConst};
	mulDel += LAMBDA([] (std::string a, std::string b) { TestFunc(a, b); });
	mulDel += LAMBDA([&] (const std::string& a, const std::string& b) { TestFunc(a, b); });
	mulDel -= &TestFuncInt;
	mulDel -= {&testClass2, &TestClass::TestMethConst};
	mulDel -= LAMBDA([] (std::string a, std::string b) { TestFunc(a, b); });
	mulDel -= LAMBDA([&] (const std::string& a, const std::string& b) { TestFunc(a, b); });
	mulDel("Finally, this message should appear ", "1 time");

	mulDel =
	{
		&TestFunc,
		{&testClass, &TestClass::TestMethConst},
		LAMBDA([] (std::string a, std::string b) { TestFunc(a, b); }),
		LAMBDA([&] (const std::string& a, const std::string& b) { TestFunc(a, b); })
	};

	for(const auto& del : mulDel)
	{
		del("This message was displayed using a foreach loop on a MulticastDelegate", "and should appear 4 times");
	}

	Predicate<> predicate =
	{
		LAMBDA([]{ return true; }),
		LAMBDA([]{ return true; }),
		LAMBDA([]{ return true; }),
		LAMBDA([]{ return true; }),
		LAMBDA([]{ return true; }),
	};

	std::cout << "should print 1: " << predicate() << '\n';

	predicate += LAMBDA([] { return false; });
	std::cout << "should print 0: " << predicate() << '\n';

	MulticastDelegate<int()> addDelegate =
	{
		LAMBDA([]() { return 1; }),
		LAMBDA([]() { return 1.; }),
		LAMBDA([]() { return 1.f; }),
		LAMBDA([]() { return 1ll; }),
	};

	std::cout << "should print 4: " << addDelegate() << '\n';

	MulticastDelegate<void(std::string)> addRemoveDelegate =
	{
		&Title,
		&a,
		&b,
		&b,
		&a,
	};
	addRemoveDelegate("Should print 'abba': ");
	std::cout << '\n';

	addRemoveDelegate = {
		&Title,
		LAMBDA([&addRemoveDelegate] (const auto&)
		       {
		       addRemoveDelegate += {&a, &b};
		       })
	};

	addRemoveDelegate("Should only print this title: ");
	addRemoveDelegate("Should print 'ab': ");
	std::cout << '\n';

	addRemoveDelegate =
	{
		&Title,
		&a,
		&b,
		&b,
		&a,
	};
	addRemoveDelegate += LAMBDA([&addRemoveDelegate] (const auto&)
	                            {
	                            addRemoveDelegate += {&a, &b};
	                            });
	addRemoveDelegate += LAMBDA([&addRemoveDelegate] (const auto&)
	                            {
	                            addRemoveDelegate -= {&a, &b};
	                            });

	addRemoveDelegate("Should again print 'abba': ");
	std::cout << '\n';
	addRemoveDelegate("Should print once more 'abba': ");
	std::cout << '\n';
}

int main(int argc, char* argv[])
{
	try
	{
		CallTestsOnParams();
		std::cout << '\n' << '\n';
		ComparisonsTests();
		std::cout << '\n' << '\n';
		MulDelCallTestsOnParams();
		std::cout << '\n' << '\n';

		constexpr long long loopCount = 100000000;
		SpeedTestWithFunc(loopCount);
		std::cout << '\n' << '\n';
		MulDelSpeedTestWithFunc(loopCount);
		std::cout << '\n' << '\n';

		SpeedTestWithMeth(loopCount);
		std::cout << '\n' << '\n';
		MulDelSpeedTestWithMeth(loopCount);
		std::cout << '\n' << '\n';
		return 0;
	}
	catch(const std::exception& e)
	{
		std::cout << e.what() << '\n';
	}
}
