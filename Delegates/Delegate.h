#pragma once
#if defined NDEBUG && NDEBUG != 0
#define IS_DEBUG 1
#else
#define IS_DEBUG 0
#endif

#include "DelegateConcepts.h"
#include "Function.h"
#include "Lambda.h"

namespace Delegate
{
	template <typename ReturnType, typename... Args>
	class Delegate;

	template <typename ReturnType, typename... Args>
	class Delegate<ReturnType(Args...)> final
	{
	private:
		FunctionBase<ReturnType, Args...>* function;

	public:
		template <typename TargetType, typename FunctionType>
		using Function = Function<TargetType, FunctionType, ReturnType, Args...>;

		Delegate() : function(nullptr) {}

		Delegate(nullptr_t) : function(nullptr) {}

		/**
		 * \brief Creates a Delegate holding the given lambda function.<br/>
		 * <br/>
		 * <b>usage:</b><br/>
		 * Delegate<void()> myDelegate = LAMBDA([*you captured vars*], (){*your lambda code*}); <br/>
		 * <br/>
		 * <b>WARNING:</b> lambdas
		 * not being natively comparable, an alternative solution has made available.
		 * This implementation might yield an erroneous "false" result while you would expect
		 * a "true" result when comparing them, though.
		 * 
		 * \tparam LambdaType The lambda type
		 * \param lambda the lambda subtype created with the LAMBDA macro
		 * 
		 * @see Lambda<T>
		 */
		template <typename LambdaType> requires AssignableToDelegate<LambdaType, ReturnType, Args...> && IsLambda<
			LambdaType>
		Delegate(const Lambda<LambdaType>& lambda) : function(new Function<void, Lambda<LambdaType>>(lambda)) {}

		/**
		 * \brief Creates a Delegate holding the given function
		 * \tparam FunctionType The function type
		 * \param func The function pointer
		 */
		template <typename FunctionType> requires AssignableToDelegate<FunctionType, ReturnType, Args...> && not
			IsLambda<FunctionType>
		Delegate(FunctionType&& func) :
			function(new Function<void, FunctionType>(std::forward<FunctionType>(func))) {}

		/**
		 * \brief Creates a Delegate holding the given method,
		 * as well as a pointer to the object to use when calling this Delegate.
		 * \tparam MethodType The method type
		 * \tparam TargetType A type on which this method is callable
		 * \param method The method pointer
		 * \param target A pointer to a valid object of type TargetType
		 */
		template <class TargetType, typename MethodType> requires AssignableToDelegate<
			MethodType, ReturnType, TargetType, Args...>
		Delegate(TargetType* target, MethodType method) :
			function(new Function<TargetType, MethodType>(target, method)) {}

		/**
		 * \brief Copy ctor
		 */
		Delegate(const Delegate& other) :
			function(other.function->Clone()) {}

		/**
		 * \brief Move ctor
		 */
		explicit Delegate(Delegate&& other) noexcept :
			function(std::move(other.function))
		{
			other.function = nullptr;
		}

		~Delegate()
		{
			delete function;
		}

		Delegate& operator=(const Delegate& other)
		{
			if(this != &other)
			{
				delete function;
				function = other.function->Clone();
			}
			return *this;
		}

		template <typename FunctionType> requires AssignableToDelegate<FunctionType, ReturnType, Args...> && not
			IsLambda<FunctionType>
		Delegate& operator=(FunctionType&& func)
		{
			delete function;
			function = new Function<void, FunctionType>(std::forward<FunctionType>(func));

			return *this;
		}

		/**
		 * \brief Creates a Delegate holding the given lambda function.<br/>
		 * <br/>
		 * <b>usage:</b><br/>
		 * Delegate<void()> myDelegate = LAMBDA([*you captured vars*], (){*your lambda code*}); <br/>
		 * <br/>
		 * <b>WARNING:</b> lambdas
		 * not being natively comparable, an alternative solution has been made available.
		 * This implementation might yield an erroneous "false" result while you would expect
		 * a "true" result when comparing them, though.
		 * 
		 * \tparam LambdaType The type of a lambda
		 * \param func Lambda subtype created with the LAMBDA macro
		 */
		template <typename LambdaType> requires AssignableToDelegate<LambdaType, ReturnType, Args...> && IsLambda<
			LambdaType>
		Delegate& operator=(const Lambda<LambdaType>& func)
		{
			delete function;
			function = new Function<void, Lambda<LambdaType>>(func);

			return *this;
		}

		/**
		 * \brief move operator=
		 */
		Delegate& operator=(Delegate&& other) noexcept
		{
			delete function;
			function = std::move(other.function);
			other.function = nullptr;

			return *this;
		}

		Delegate& operator=(nullptr_t)
		{
			delete function;
			function = nullptr;

			return *this;
		}

		[[nodiscard]] ReturnType Invoke(const Args&... args) const
		{
			if constexpr(IS_DEBUG)
				if(function == nullptr)
					throw std::exception("Delegate invoked with no function bound");

			return static_cast<ReturnType>(function->Invoke(args...));
		}

		[[nodiscard]] ReturnType operator()(const Args&... args) const
		{
			return Invoke(args...);
		}

		/// \return true if it holds a function, false otherwise. equivalent to `thisDelegate != nullptr`
		[[nodiscard]] operator bool() const { return (*this) != nullptr; }

		/**
		 * \return true if both Delegate hold the same function pointer (and same target, if applicable),
		 * false otherwise
		 */
		[[nodiscard]] friend bool operator==(const Delegate& left,
		                                     const Delegate& right)
		{
			return left.function == right.function
				|| (left.function != nullptr
					&& right.function != nullptr
					&& (*(left.function)) == (*(right.function)));
		}

		/// \return true if it holds a function, false otherwise.
		[[nodiscard]] friend bool operator==(const Delegate& left,
		                                     const nullptr_t&)
		{
			return left.IsNull();
		}

		[[nodiscard]] void* GetTarget() const
		{
			return function->GetTarget();
		}

		template <typename T>
		[[nodiscard]] T* GetTargetAs() const
		{
			return function->template GetTarget<T>();
		}

	protected:
		[[nodiscard]] bool IsNull() const
		{
			return function == nullptr;
		}
	};
}

#undef IS_DEBUG