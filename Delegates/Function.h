#pragma once
#include "Lambda.h"

namespace Delegate
{
	template <typename ReturnType, typename... Args>
	class FunctionBase
	{
	public:
		virtual ~FunctionBase() = default;

		FunctionBase(const FunctionBase& other) = delete;
		FunctionBase(FunctionBase&& other) noexcept = delete;
		FunctionBase& operator=(const FunctionBase& other) = delete;
		FunctionBase& operator=(FunctionBase&& other) noexcept = delete;

		[[nodiscard]] virtual ReturnType Invoke(const Args&... args) const = 0;
		[[nodiscard]] virtual FunctionBase* Clone() = 0;
		[[nodiscard]] virtual bool operator==(const FunctionBase& other) const = 0;

		template <typename T>
		[[nodiscard]] T* GetTarget() const
		{
			return static_cast<T*>(GetTarget());
		}

	protected:
		FunctionBase() = default;
		[[nodiscard]] virtual void* GetTarget() const = 0;
	};

	/**
	 * \brief The Method version of the Function class. Used to add a level of abstraction to callable stuff.
	 * \tparam TargetType The type to which the callable data is bound to
	 * \tparam MethodType The type of the callable data to store
	 * \tparam ReturnType The return type of the call
	 * \tparam Args The arguments type to pass when calling
	 */
	template <typename TargetType, typename MethodType, typename ReturnType, typename... Args>
	class Function : public FunctionBase<ReturnType, Args...>
	{
	public:
		static constexpr bool isLambda = false;

		Function(TargetType* target, MethodType method) : target(target), function(method) {}
		Function(const Function& other) = default;
		Function(Function&& other) = default;

		Function& operator=(const Function& other) = default;
		Function& operator=(Function&& other) noexcept = default;

		~Function() override = default;

		[[nodiscard]] ReturnType Invoke(const Args&... args) const override
		{
			return static_cast<ReturnType>((target->*function)(args...));
		}

		[[nodiscard]] FunctionBase<ReturnType, Args...>* Clone() override
		{
			return new Function(target, function);
		}

		[[nodiscard]] void* GetTarget() const override
		{
			return const_cast<std::remove_const_t<TargetType>*>(target);
		}

		[[nodiscard]] bool operator==(const FunctionBase<ReturnType, Args...>& other) const override
		{
			const auto* const casted = dynamic_cast<const Function* const>(
				&other);
			return casted != nullptr && *this == *casted;
		}

		[[nodiscard]] bool operator==(const Function& other) const
		{
			return target == other.target && function == other.function;
		}

		TargetType* target;
		MethodType function;
	};

	/**
     * \brief The Function version of the Function class. Used to add a level of abstraction to callable stuff.
     * \tparam FunctionType The type of the callable data to store
     * \tparam ReturnType The return type of the call
     * \tparam Args The arguments type to pass when calling
     */
	template <typename FunctionType, typename ReturnType, typename... Args>
	class Function<void, FunctionType, ReturnType, Args...> : public FunctionBase<ReturnType, Args...>
	{
	public:
		static constexpr bool isLambda = IsLambda<FunctionType>;

		Function(FunctionType func) : function(func) {}

		Function(const Function& other) = default;
		Function(Function&& other) = default;

		Function& operator=(const Function& other) = default;
		Function& operator=(Function&& other) noexcept = default;

		~Function() override = default;

		[[nodiscard]] ReturnType Invoke(const Args&... args) const override
		{
			return static_cast<ReturnType>(function(args...));
		}

		[[nodiscard]] FunctionBase<ReturnType, Args...>* Clone() override
		{
			return new Function(function);
		}

		[[nodiscard]] void* GetTarget() const override
		{
			return nullptr;
		}

		[[nodiscard]] bool operator==(const FunctionBase<ReturnType, Args...>& other) const override
		{
			const auto* const casted = dynamic_cast<const Function* const>(&
				other);
			return casted != nullptr && *this == *casted;
		}

		[[nodiscard]] bool operator==(const Function& other) const
		{
			return AreFuncEquals(function, other.function);
		}

		FunctionType function;

	private:
		template <typename FuncType>
		static bool AreFuncEquals(const FuncType& left, const FuncType& right)
		{
			return false;
		}

		template <typename FuncType> requires std::equality_comparable<FuncType>
		static bool AreFuncEquals(const FuncType& left, const FuncType& right)
		{
			return left == right;
		}
	};

	/**
	 * \brief The Lambda version of the Function class. Used to add a level of abstraction to callable stuff.
	 * \tparam LambdaType The exact type of the Lambda wrapper object instance
	 * \tparam ReturnType The return type of the call
	 * \tparam Args The arguments type to pass when calling
	 * \see LAMBDA(...)
	 */
	template <typename LambdaType, typename ReturnType, typename... Args>
	class Function<void, Lambda<LambdaType>, ReturnType, Args...> : public FunctionBase<ReturnType, Args...>,
	                                                                public HashHolder
	{
	public:
		static constexpr bool isLambda = IsLambda<LambdaType>;

		Function(Lambda<LambdaType> func) : HashHolder(func), function(std::move(func)) {}

		Function(const Function& other) = default;
		Function(Function&& other) = default;

		Function& operator=(const Function& other) = default;
		Function& operator=(Function&& other) noexcept = default;

		~Function() override = default;

		[[nodiscard]] ReturnType Invoke(const Args&... args) const override
		{
			return static_cast<ReturnType>(function.lambda(args...));
		}

		[[nodiscard]] FunctionBase<ReturnType, Args...>* Clone() override
		{
			return new Function(function);
		}

		[[nodiscard]] void* GetTarget() const override
		{
			return nullptr;
		}

		[[nodiscard]] bool operator==(const FunctionBase<ReturnType, Args...>& other) const override
		{
			const auto* const casted = dynamic_cast<const HashHolder*const>(&other);
			return casted != nullptr && static_cast<HashHolder>(*this) == *casted;
		}

		[[nodiscard]] bool operator==(const Function& other) const
		{
			return function == other.function;
		}

		Lambda<LambdaType> function;
	};
}
