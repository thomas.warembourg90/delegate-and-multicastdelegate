#pragma once
#include <vector>

#include "Delegate.h"

namespace Delegate
{
	template <typename ReturnType, typename... Args>
	class MulticastDelegate;

	template <typename ReturnType, typename... Args>
	class MulticastDelegate<ReturnType(Args...)> final
	{
	public:
		using AcceptedDelegateType = Delegate<ReturnType(Args...)>;

		MulticastDelegate() = default;

		MulticastDelegate(const std::initializer_list<Delegate<ReturnType(Args...)>>& initList)
		{
			Set(initList);
		}

		MulticastDelegate(const AcceptedDelegateType& other)
		{
			Set(other);
		}

		MulticastDelegate(const MulticastDelegate& other) = default;
		MulticastDelegate(MulticastDelegate&& other) noexcept = default;
		~MulticastDelegate() = default;

		bool IsNull() const
		{
			return delegates.empty() && addBuffer.empty();
		}

		void AddListener(const AcceptedDelegateType& delegate)
		{
			std::vector<AcceptedDelegateType> toAdd = {delegate};
			AddListener(toAdd);
		}

		void AddListener(const MulticastDelegate& multicastDelegate)
		{
			AddListener(multicastDelegate.delegates);
		}

		void RemoveListener(const AcceptedDelegateType& delegate)
		{
			std::vector<AcceptedDelegateType> toRemove = {delegate};
			RemoveListener(toRemove);
		}

		void RemoveListener(const MulticastDelegate& multicastDelegate)
		{
			RemoveListener(multicastDelegate.delegates);
		}

		[[nodiscard]] ReturnType Invoke(const Args&... args) const
		{
			return InvokeAll<ReturnType>(args...);
		}

		[[nodiscard]] ReturnType operator()(const Args&... args) const
		{
			return Invoke(args...);
		}

		MulticastDelegate& operator+=(const AcceptedDelegateType& toAdd)
		{
			AddListener(toAdd);
			return *this;
		}

		MulticastDelegate& operator+=(
			const std::initializer_list<AcceptedDelegateType>& toAdd)
		{
			AddListener(static_cast<std::vector<AcceptedDelegateType>>(toAdd));
			return *this;
		}

		MulticastDelegate& operator-=(const AcceptedDelegateType& toRemove)
		{
			RemoveListener(toRemove);
			return *this;
		}

		MulticastDelegate& operator-=(
			const std::initializer_list<AcceptedDelegateType>& toRemove)
		{
			RemoveListener(static_cast<std::vector<AcceptedDelegateType>>(toRemove));
			return *this;
		}

		MulticastDelegate& operator=(const AcceptedDelegateType& delegate)
		{
			Set(delegate);
			return *this;
		}

		MulticastDelegate& operator=(const MulticastDelegate& other)
		{
			Set(other);
			return *this;
		}

		MulticastDelegate& operator=(MulticastDelegate&& other) noexcept = default;


		operator bool() const
		{
			return (*this) != nullptr;
		}

		friend bool operator==(const MulticastDelegate& left,
		                       const nullptr_t&)
		{
			return left.IsNull();
		}

		void Clear()
		{
			addBuffer.clear();
			removeBuffer.clear();
			delegates.clear();
		}

#pragma region Iterators

		struct Iterator
		{
			using iterator_category = std::forward_iterator_tag;
			using difference_type = typename std::vector<Delegate<ReturnType(Args...)>>::iterator::difference_type;
			using value_type = typename std::vector<Delegate<ReturnType(Args...)>>::iterator::value_type;
			using pointer = typename std::vector<Delegate<ReturnType(Args...)>>::iterator::pointer;
			using reference = typename std::vector<Delegate<ReturnType(Args...)>>::iterator::reference;

			Iterator() = default;

			Iterator(pointer ptr) : ptr(ptr) {}

			Iterator(pointer ptr, MulticastDelegate* owner) : ptr(ptr), owner(owner)
			{
				if(owner != nullptr)
				{
					owner->isBeingInvoked = true;
				}
			}

			Iterator(const Iterator& other) = default;
			Iterator(Iterator&& other) = default;
			Iterator& operator=(const Iterator& other) = default;
			Iterator& operator=(Iterator&& other) = default;

			~Iterator()
			{
				if(owner != nullptr)
				{
					owner->isBeingInvoked = false;
					owner->HandleBuffers();
				}
			}

			reference operator*() const { return *ptr; }
			pointer operator->() { return ptr; }

			// Prefix increment
			Iterator& operator++()
			{
				++ptr;
				return *this;
			}

			// Postfix increment
			Iterator operator++(int)
			{
				Iterator tmp = *this;
				++(*this);
				return tmp;
			}

			friend bool operator==(const Iterator& a, const Iterator& b) { return a.ptr == b.ptr; }
			friend bool operator!=(const Iterator& a, const Iterator& b) { return a.ptr != b.ptr; }

		private:
			pointer ptr = nullptr;
			MulticastDelegate* owner = nullptr;
		};

		Iterator begin() noexcept
		{
			return delegates.empty()
				       ? nullptr
				       : delegates.data();
		}

		Iterator end() noexcept
		{
			return Iterator((delegates.empty()
				                 ? nullptr
				                 : (&(delegates[delegates.size() - 1])) + 1), this);
		}

		struct ConstIterator
		{
			using iterator_category = std::forward_iterator_tag;
			using difference_type = typename std::vector<Delegate<ReturnType
				(Args...)>>::const_iterator::difference_type;
			using value_type = typename std::vector<Delegate<ReturnType(Args...)>>::const_iterator::value_type;
			using pointer = typename std::vector<Delegate<ReturnType(Args...)>>::const_iterator::pointer;
			using reference = typename std::vector<Delegate<ReturnType(Args...)>>::const_iterator::reference;

			ConstIterator() = default;

			ConstIterator(pointer ptr) : ptr(ptr) {}

			ConstIterator(pointer ptr, const MulticastDelegate* owner) : ptr(ptr), owner(owner)
			{
				if(owner != nullptr)
				{
					owner->isBeingInvoked = true;
				}
			}

			ConstIterator(const ConstIterator& other) = default;
			ConstIterator(ConstIterator&& other) = default;
			ConstIterator& operator=(const ConstIterator& other) = default;
			ConstIterator& operator=(ConstIterator&& other) = default;

			~ConstIterator()
			{
				if(owner != nullptr)
				{
					owner->isBeingInvoked = false;
					const_cast<MulticastDelegate*>(owner)->HandleBuffers();
				}
			}

			reference operator*() const { return *ptr; }
			pointer operator->() { return ptr; }

			// Prefix increment
			ConstIterator& operator++()
			{
				++ptr;
				return *this;
			}

			// Postfix increment
			ConstIterator operator++(int)
			{
				Iterator tmp = *this;
				++(*this);
				return tmp;
			}

			friend bool operator==(const ConstIterator& a, const ConstIterator& b) { return a.ptr == b.ptr; }
			friend bool operator!=(const ConstIterator& a, const ConstIterator& b) { return a.ptr != b.ptr; }

		private:
			pointer ptr = nullptr;
			const MulticastDelegate* owner = nullptr;
		};

		ConstIterator begin() const noexcept
		{
			return delegates.empty()
				       ? nullptr
				       : delegates.data();
		}
		ConstIterator cbegin() const noexcept { return begin(); }

		ConstIterator end() const noexcept
		{
			return ConstIterator((delegates.empty()
				                      ? nullptr
				                      : (&(delegates[delegates.size() - 1])) + 1), this);
		}
		ConstIterator cend() const noexcept { return end(); }

#pragma endregion

	protected:
		void AddListener(const std::vector<AcceptedDelegateType>& toAdd)
		{
			std::vector<AcceptedDelegateType>& bufferToUse = (isBeingInvoked
				                                                  ? addBuffer
				                                                  : delegates);

			for(const auto& delegate : toAdd)
			{
				bufferToUse.push_back(delegate);
			}

			if(!isBeingInvoked)
			{
				addBuffer.clear();
			}
		}

		void RemoveListener(const std::vector<AcceptedDelegateType>& toRemove)
		{
			// we create a map with an int counter as value of how many times the key should be deleted
			std::vector<std::pair<AcceptedDelegateType, int>> toRemoveDelegates;
			for(size_t i = 0; i < toRemove.size(); ++i)
			{
				auto find = std::find_if(
					toRemoveDelegates.begin(),
					toRemoveDelegates.end(),
					[&toRemove, &i](const auto& pair)
					{
						return toRemove[i] == pair.first;
					}
				);

				if(find != toRemoveDelegates.end())
					++find->second;
				else
					toRemoveDelegates.emplace_back(toRemove[i], 1);
			}

			// we check for each element of the corresponding buffer if it is in the map and if it is still to remove
			std::vector<Delegate<ReturnType(Args...)>>& bufferToUse = isBeingInvoked
				                                                          ? addBuffer
				                                                          : delegates;
			std::erase_if(bufferToUse, [&toRemoveDelegates](auto& del)
			{
				auto find = std::find_if(toRemoveDelegates.begin(), toRemoveDelegates.end(), [&del](const auto& pair)
				{
					return pair.first == del;
				});
				if(find != toRemoveDelegates.end() && find->second > 0)
				{
					--find->second;
					return true;
				}
				return false;
			});

			if(isBeingInvoked)
			{
				// the MulticastDelegate is being invoked,
				// so we need to store what we couldn't remove from the addBuffer
				for(const auto& pair : toRemoveDelegates)
				{
					for(int i = 0; i < pair.second; ++i)
					{
						removeBuffer.push_back(pair.first);
					}
				}
			}
			else
			{
				// we've just removed from the main buffer, we can clear the removeBuffer
				removeBuffer.clear();
			}
		}

		void HandleBuffers()
		{
			AddListener(addBuffer);
			RemoveListener(removeBuffer);
		}

		template <typename RT = ReturnType> requires std::same_as<RT, void>
		void InvokeAll(const Args&... args) const
		{
			for(auto b = begin(), e = end(); b != e; ++b)
			{
				if(*b != nullptr)
				{
					b->Invoke(args...);
				}
			}
		}

		template <typename RT> requires IsAddable<RT> && not std::same_as<RT, bool>
		ReturnType InvokeAll(const Args&... args) const
		{
			auto b = begin(), e = end();
			ReturnType result = b->Invoke(args...);
			for(++b; b != e; ++b)
			{
				result = result + b->Invoke(args...);
			}

			return result;
		}

		template <typename RT> requires std::same_as<RT, bool>
		bool InvokeAll(const Args&... args) const
		{
			auto b = begin(), e = end();
			bool result = b->Invoke(args...);
			for(++b; b != e; ++b)
			{
				result = result & b->Invoke(args...);
			}

			return result;
		}

		void Set(nullptr_t null)
		{
			if(isBeingInvoked)
			{
				throw std::exception("Can't use operator= from a registered function to this MulticastDelegate");
			}
			delegates.clear();
			addBuffer.clear();
			removeBuffer.clear();
		}

		void Set(const AcceptedDelegateType& delegate)
		{
			if(isBeingInvoked)
			{
				throw std::exception("Can't use operator= from a registered function to this MulticastDelegate");
			}
			Set(nullptr);
			AddListener({delegate});
		}

		void Set(const MulticastDelegate& other)
		{
			if(isBeingInvoked)
			{
				throw std::exception("Can't use operator= from a registered function to this MulticastDelegate");
			}
			Set(nullptr);
			AddListener(other.delegates);
		}

		void Set(const std::initializer_list<AcceptedDelegateType>& initList)
		{
			if(isBeingInvoked)
			{
				throw std::exception("Can't use operator= from a registered function to this MulticastDelegate");
			}
			Set(nullptr);
			AddListener(std::vector<AcceptedDelegateType>(initList));
		}

		mutable bool isBeingInvoked = false;
		mutable std::vector<Delegate<ReturnType(Args...)>> delegates;
		mutable std::vector<Delegate<ReturnType(Args...)>> addBuffer;
		mutable std::vector<Delegate<ReturnType(Args...)>> removeBuffer;
	};
}
