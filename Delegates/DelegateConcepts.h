#pragma once
#include <concepts>

namespace Delegate
{
	template <typename FunctionType, typename ReturnType, typename... Args>
	concept AssignableToDelegate = std::is_invocable_r_v<ReturnType, FunctionType, Args...>;

	template <typename FunctionType>
	concept IsLambda = std::is_class_v<std::remove_reference_t<FunctionType>>;

	template <typename T>
	concept IsAddable = requires(T a, T b) { a + b; };

	template <typename T>
	concept IsLambdaCifyable = IsLambda<T> && requires(T a) { +a; };

	template <class Ty1, class Ty2>
	concept NotSameAs = !std::same_as<std::remove_cvref_t<Ty1>, std::remove_cvref_t<Ty2>>;
}
