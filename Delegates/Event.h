#pragma once
#include "MulticastDelegate.h"

namespace Delegate
{
	template <typename ReturnType, typename... Args>
	class Event;

	template <typename ReturnType, typename... Args>
	class Event<ReturnType(Args...)>
	{
	public:
		using DelegateType = typename MulticastDelegate<ReturnType(Args...)>::AcceptedDelegateType;

		Event() = delete;
		Event(const Event& other) = delete;
		Event(Event&& other) noexcept = delete;

		Event(MulticastDelegate<ReturnType(Args...)>& backingDelegate) :
			backingDelegate(backingDelegate)
		{
		}

		~Event() = default;

		Event& operator=(const Event&) = delete;
		Event& operator=(Event&&) noexcept = delete;

		void AddListener(const DelegateType& delegate)
		{
			backingDelegate.AddListener(delegate);
		}

		Event& operator+=(const DelegateType& delegate)
		{
			backingDelegate.AddListener(delegate);
			return *this;
		}

		Event& operator+=(const Event& other)
		{
			backingDelegate += other.backingDelegate;
			return *this;
		}

		void RemoveListener(const DelegateType& delegate)
		{
			backingDelegate.RemoveListener(delegate);
		}

		Event& operator-=(const DelegateType& delegate)
		{
			backingDelegate.RemoveListener(delegate);
			return *this;
		}

		Event& operator-=(const Event& other)
		{
			backingDelegate -= other.backingDelegate;
			return *this;
		}

	protected:
		MulticastDelegate<ReturnType(Args...)>& backingDelegate;
	};

	template <typename R, typename... Args>
	Event(MulticastDelegate<R(Args...)> backing) -> Event<R(Args...)>;

	template <typename... Args>
	using Action = MulticastDelegate<void(Args...)>;

	template <typename... Args>
	using Predicate = MulticastDelegate<bool(Args...)>;
}
