# Delegate and MulticastDelegate in C++

## Description

This project is a **personal project** initialy intended as a "skill up" one. I came up with something I'm rather proud of, 
and so I am sharing it. You are free to try it, and I would appreciate any feedback about it!

This library is a **header-only** C++20 delegate and multi-cast delegate implementation with a very C#-like synthax.
The main goals of this project are for now to make the most optimised yet easy-to-use version of the delegates in C++.
This might very well not be buildable with anything else than MSVC, as cross-platform and cross-compiling were not 
in my mind when I made this.

**Do not use it for anything serious please**; this implementation is very likely to hide nasty **bugs** here and there, 
and I don't want anyone doing money using my work without my explicit agreement.

## Notes

The demo uses boost::function for speed tests comparisons, but is in any case required to use any of this library's 
classes.